# Copyright 2017-2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=greenbone project=gsa tag=v${PV} ] \
    cmake \
    python [ blacklist=none multibuild=false ] \
    systemd-service

SUMMARY="Greenbone Security Assistant"
HOMEPAGE+=" http://www.openvas.org"

LICENCES="BSD-3 GPL-2 LGPL-2 MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-lang/node[>=8.0]
        dev-node/yarn[>=1.0]
        dev-python/polib[python_abis:*(-)?]
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.42]
        dev-libs/gnutls[>=3.2.15]
        dev-libs/libgcrypt
        dev-libs/libxml2:2.0
        net-analyzer/gvm-libs[>=11.0.0]
        net-libs/libmicrohttpd[>=0.9.0][ssl]
    run:
        net-analyzer/gvmd[>=9.0.0]
        net-analyzer/openvas-scanner[>=7.0.0]
        net-analyzer/ospd-openvas[>=1.0.0]
    test:
        dev-util/cppcheck
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCLANG_FORMAT:BOOL=FALSE
    -DCMAKE_BUILD_TYPE:STRING=Release
    -DCMAKE_DISABLE_FIND_PACKAGE_Git:BOOL=TRUE
    -DDATADIR:PATH=/usr/share
    -DDOXYGEN_EXECUTABLE:BOOL=FALSE
    -DGSAD_PID_DIR:PATH=/run
    -DGVM_RUN_DIR:PATH=/run/gvm
    -DLIBDIR:PATH=/usr/$(exhost --target)/lib
    -DLOCALSTATEDIR:PATH=/var
    -DPYTHON_EXECUTABLE:PATH=${PYTHON}
    -DSBINDIR:PATH=/usr/$(exhost --target)/bin
    -DSYSCONFDIR:PATH=/etc
    -DXMLMANTOHTML_EXECUTABLE:BOOL=FALSE
    -DXMLTOMAN_EXECUTABLE:BOOL=FALSE
)

src_prepare() {
    cmake_src_prepare

    # for now we keep installing our own
    edo sed \
        -e '/add_subdirectory (config)/d' \
        -i gsad/CMakeLists.txt
}

src_compile() {
    # https://github.com/greenbone/gsa/issues/1261
    esandbox disable_net
    default
    esandbox enable_net
}

src_install() {
    cmake_src_install

    keepdir /var/log/gvm

    install_systemd_files

    insinto /etc/conf.d
    hereins gsad.conf << EOF
#GSAD_SSL_CERT=--ssl-certificate=/etc/ssl/openvas.crt
#GSAD_SSL_KEY=--ssl-private-key=/etc/ssl/openvas.key
EOF
}

