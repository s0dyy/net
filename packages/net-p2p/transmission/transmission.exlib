# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2010, 2012 Ingmar Vanhassel
# Copyright 2010, 2011 Sterling X. Winter <replica@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

if ever is_scm; then
    require github
fi

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]
require qmake systemd-service [ systemd_files=[ daemon/transmission-daemon.service ] ]
require freedesktop-desktop gtk-icon-cache

export_exlib_phases src_prepare src_configure src_compile src_install pkg_postinst pkg_postrm

SUMMARY="Fast, easy, and free BitTorrent client"
DESCRIPTION="
Transmission is a cross-platform BitTorrent client. Features include:
 * Uses fewer resources than other clients
 * Native Mac, GTK+ and Qt GUI clients
 * Daemon ideal for servers, embedded systems, and headless use
 * All these can be remote controlled by Web and Terminal clients
 * Bluetack (PeerGuardian) blocklists with automatic updates
 * Full encryption, DHT, PEX and Magnet Link support
"
HOMEPAGE="https://www.transmissionbt.com"

if ! ever is_scm; then
    DOWNLOADS="https://github.com/${PN}/${PN}-releases/raw/master/${PNV}.tar.xz"
fi

LICENCES="GPL-2 MIT"
SLOT="0"
MYOPTIONS="
    gtk [[ description = [ Build the GTK+ frontend ] ]]
    kde [[
        description = [ Register Qt frontend as a magnet link handler for KDE ]
        requires = [ qt5 ]
    ]]
    qt5 [[ description = [ Build the Qt frontend ] ]]
    systemd
    ( linguas: an ar ast az be be@latin bg bn bo br bs ca ca@valencia ceb ckb cs da de el en_AU
               en_CA en_GB eo es et eu fa fi fil fo fr ga gl gv he hi hr hu hy ia id is it ja ka
               kk ko ku ky li lt lv mk ml mr ms mt my nb nds nl nn oc pa pl pt pt_BR ro ru si sk
               sl sq sr sv sw ta_LK te th tl tr ug uk ur uz vi zh_CN zh_TW )
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-libs/glib:2 [[ note = [ for glib-gettext.m4 ] ]]
        dev-util/intltool[>=0.35.0]
        sys-devel/gettext
        virtual/pkg-config[>=0.9.0]
        qt5? ( x11-libs/qttools:5 )
    build+run:
        net-dns/libidn2:=
        dev-libs/libevent:=[>=2.0.10]
        net-libs/miniupnpc[>=1.7]
        net-libs/libnatpmp[>=20110808]
        net-misc/curl[>=7.15.4]
        group/transmission
        user/transmission
        gtk? (
            dev-libs/dbus-glib:1[>=0.70]
            dev-libs/glib:2[>=2.32.0]
            x11-libs/gtk+:3[>=3.4.0]
        )
        qt5? ( x11-libs/qtbase:5[gui(+)] )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl[>=0.9.7] )
        systemd? ( sys-apps/systemd )
"

AT_M4DIR=( m4 )

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-cli # Deprecated.
    --enable-daemon
    --enable-external-natpmp
    --enable-nls
    --enable-utp
    --disable-mac
    --with-crypto=openssl
    --with-inotify
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( 'gtk' 'systemd systemd-daemon' )

transmission_src_prepare() {
    # TODO(keruspe): Push upstream.
    edo sed -i 's/libsystemd-daemon/libsystemd/' configure.ac
    edo rm m4/glib-gettext.m4 # Needed to build against new GLib
    edo intltoolize --automake --copy --force
    autotools_src_prepare

    # Set the right icon and translation path.
    if option qt5; then
        edo sed -i '/^Icon=/ s/$/-qt/' qt/transmission-qt.desktop
        edo sed \
            -e 's:applicationDirPath () + ::g' \
            -e 's:/translations:/usr/share/transmission/translations:g' \
            -i qt/Application.cc
    fi
}

transmission_src_configure() {
    default

    if option qt5; then
        edo pushd qt
        eqmake 5 qtr.pro
        edo popd
    fi
}

transmission_src_compile() {
    default

    if option qt5; then
        edo pushd qt
        edo lrelease-qt5 translations/*.ts
        emake
        edo popd
    fi
}

transmission_src_install() {
    default

    install_systemd_files

    keepdir /var/lib/transmission
    edo chown transmission:transmission "${IMAGE}"/var/lib/transmission

    if option kde; then
        insinto /usr/share/kservices5
        hereins transmission-magnet.protocol <<EOF
[Protocol]
protocol=magnet
exec=transmission-qt '%u'
Icon=transmission-qt
input=none
output=none
helper=true
listing=
reading=false
writing=false
makedir=false
deleting=false
EOF
    fi

    if option qt5; then
        edo pushd qt

        emake INSTALL_ROOT="${IMAGE}"/usr/$(exhost --target) install

        insinto /usr/share/${PN}/translations
        doins translations/*.qm

        insinto /usr/share/applications
        doins transmission-qt.desktop

        newdoc README.txt README-Qt

        edo popd

        insinto /usr/share/icons/hicolor/scalable/apps
        newins gtk/icons/hicolor_apps_scalable_transmission.svg transmission-qt.svg
    fi

    edo rm -f "${IMAGE}"/usr/share/${PN}/web/LICENSE
}

transmission_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

transmission_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}
