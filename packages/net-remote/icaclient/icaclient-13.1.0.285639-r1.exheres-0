# Copyright 2014 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'icaclient-12.1.0.ebuild' from Gentoo, which is:
#     Copyright 1999-2014 Gentoo Foundation.

MY_PNV="${PNV/-/_}"

require freedesktop-desktop

SUMMARY="ICA Client for Citrix Presentation servers"
DESCRIPTION="
"
HOMEPAGE="http://www.citrix.com/downloads/citrix-receiver/linux"
DOWNLOADS="
    manual:
        platform:amd64? ( ${MY_PNV}_amd64.deb )
        platform:x86?   ( ${MY_PNV}_i386.deb )
"

LICENCES="
    LGPL-2.1 [[ note = [ bundled version of FFmpeg ] ]]
    icaclient
"
SLOT="0"
PLATFORMS="-* ~amd64 ~x86"
MYOPTIONS="
    platform: amd64 x86
"

DEPENDENCIES="
    build+run:
        app-misc/ca-certificates
    run:
        app-crypt/krb5 [[ note = [ also needs 32bit version, libkcpm.so needs libgssapi_krb5.so.2 ] ]]
        dev-libs/glib:2 [[ note = [ also needs 32bit version ] ]]
        dev-libs/libxml2:2.0 [[ note = [ for selfservice ] ]]
        dev-libs/xerces-c [[ note = [ for PrimaryAuthManager,ServiceRecord ] ]]
        media-libs/gstreamer:0.10 [[ note = [ for libgstflatstm,gst_aud_play,gst_aud_read,gst_play,gst_read ] ]]
        media-libs/libpng:1.2 [[ note = [ also needs 32bit version, for pnabrowse,selfservice ] ]]
        media-libs/libvorbis [[ note = [ also needs 32bit version, for VORBIS.DLL ] ]]
        media-plugins/gst-plugins-base:0.10 [[ note = [ for gst_aud_play,gst_aud_read,gst_play,gst_read ] ]]
        net-dns/libidn
        x11-libs/gdk-pixbuf:2.0 [[ note = [ also needs 32bit version ] ]]
        x11-libs/gtk+:2 [[ note = [ also needs 32bit version ] ]]
        x11-libs/libX11 [[ note = [ also needs 32bit version ] ]]
        x11-libs/libXaw [[ note = [ also needs 32bit version ] ]]
        x11-libs/libXext [[ note = [ also needs 32bit version ] ]]
        x11-libs/libXinerama [[ note = [ also needs 32bit version ] ]]
        x11-libs/libXmu [[ note = [ also needs 32bit version ] ]]
        x11-libs/libXrender [[ note = [ also needs 32bit version ] ]]
        x11-libs/libXt [[ note = [ also needs 32bit version ] ]]
"

RESTRICT="strip"

WORK=${WORKBASE}

pkg_setup() {
    exdirectory --allow /opt
}

src_unpack() {
    default
    edo tar xf data.tar.gz
}

src_install() {
    ICAINSTROOT="/opt/Citrix/ICAClient"
    ICASRCROOT="opt/Citrix/ICAClient"

    doins -r etc usr

    insinto /usr/share/mime/packages
    doins "${ICASRCROOT}"/desktop/Citrix-mime_types.xml

    insinto "${ICAINSTROOT}"
    doins -r "${ICASRCROOT}"/{gtk,icons,keyboard,nls}

    exeinto "${ICAINSTROOT}"
    doexe "${ICASRCROOT}"/*.DLL
    doexe "${ICASRCROOT}"/{libctxssl,libproxy,npica}.so
    doexe "${ICASRCROOT}"/{AuthManagerDaemon,selfservice,ServiceRecord,PrimaryAuthManager,wfica}

    exeinto "${ICAINSTROOT}"/lib
    doexe "${ICASRCROOT}"/lib/*.so

    insinto "${ICAINSTROOT}"
    doins "${ICASRCROOT}"/nls/en/eula.txt

    insinto "${ICAINSTROOT}"/config
    doins "${ICASRCROOT}"/config/* "${ICASRCROOT}"/config/.* "${ICASRCROOT}"/nls/en/*.ini
    doins -r "${ICASRCROOT}"/config/usertemplate

    dodir "${ICAINSTROOT}"/keystore
    dosym /etc/ssl/certs "${ICAINSTROOT}"/keystore/cacerts

    insinto "${ICAINSTROOT}"/util
    doins "${ICASRCROOT}"/util/pac.js

    exeinto "${ICAINSTROOT}"/util
    doexe "${ICASRCROOT}"/util/{configmgr,conncenter,echo_cmd,gst_aud_play,gst_aud_read,hdxcheck.sh,icalicense.sh}
    doexe "${ICASRCROOT}"/util/{nslaunch,pacexec,pnabrowse,storebrowse,sunraymac.sh,what,xcapture}
    doexe "${ICASRCROOT}"/util/{gst_{play,read},libgstflatstm.so}

    insinto /etc/env.d
    hereins 10ICAClient <<EOF
PATH=/opt/Citrix/ICAClient
ROOTPATH=/opt/Citrix/ICAClient
ICAROOT=/opt/Citrix/ICAClient
EOF

    dodir /opt/netscape/plugins
    dosym /opt/Citrix/ICAClient/npica.so /opt/netscape/plugins/npica.so
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
}

